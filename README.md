#  Challenge Zul Digital
**Paulo Henrique de Siqueira**

paulohenriqu@hotmail.com

https://www.linkedin.com/in/paulohenriquesiqueira/


# Tecnologias utilizadas
As seguintes tecnologias foram utilizadas nesta solução:
* Java 
* Spring Boot
* Maven
* Docker

# Executando o projeto
* Clone este repositório
* Na pasta raíz do projeto rode os comandos
	*    docker build -t zuldigital/engineer-exam .
	*  docker run --rm zuldigital/engineer-exam