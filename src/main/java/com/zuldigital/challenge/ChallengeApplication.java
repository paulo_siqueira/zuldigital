package com.zuldigital.challenge;

import java.util.List;
import java.util.Random;

import com.zuldigital.challenge.service.TweetService;
import com.zuldigital.challenge.service.dto.TweetDTO;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengeApplication.class, args);
	}

	@Bean
	public CommandLineRunner run(RestTemplate restTemplate, TweetService tweetService) throws Exception {
		return args -> {
			Random rand = new Random(); 
			List<TweetDTO> tweets=tweetService.getTweets();
			TweetDTO randomTweet= tweets.get(rand.nextInt(tweets.size())); 
			tweetService.printTweet(randomTweet);
		};
	}

}
