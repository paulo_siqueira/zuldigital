package com.zuldigital.challenge.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zuldigital.challenge.service.dto.TweetDTO;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.BasicJsonParser;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class TweetService {
    RestTemplate restTemplate;
    @Value("${tweetApi.url}")
    String baseURL;
    @Value("${tweetApi.version}")
    String apiVersion;

    public TweetService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getToken() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> httpEntity = new HttpEntity<String>(headers);
        ResponseEntity<String> res = restTemplate.exchange(baseURL + apiVersion + "/auth", HttpMethod.POST, httpEntity,
                String.class);
        Map<String, Object> map = new BasicJsonParser().parseMap(res.getBody());
        return (String) map.get("token");
    }

    public List<TweetDTO> getTweets() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken());
        HttpEntity<?> entity = new HttpEntity<>(headers);
        ResponseEntity<List<TweetDTO>> response = restTemplate.exchange(
                baseURL + apiVersion + "/statuses/home_timeline.json", HttpMethod.GET, entity,
                new ParameterizedTypeReference<List<TweetDTO>>() {
                });
        List<TweetDTO> tweets = response.getBody();
        return tweets;
    }

    public void printTweet(TweetDTO tweet) {

        String[] splitted = tweet.getText().split("(?<=\\G.{45})");

        for (int i = 0; i < splitted.length; i++) {
            String tweetCounter = "Tweet #" + (i + 1) + ": ";
            String output = tweetCounter + splitted[i];
            System.out.println(output);
        }

    }

}