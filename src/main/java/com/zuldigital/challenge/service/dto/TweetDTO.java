package com.zuldigital.challenge.service.dto;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TweetDTO{

    long id;
    String id_str;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "EEE MMM dd HH:mm:ss Z yyyy", locale = "en")
    ZonedDateTime created_at;
    UserDTO user;
    String text;

    public TweetDTO(){

    }

    public TweetDTO(long id, String id_str, String text, UserDTO user) {
        this.id = id;
        this.id_str = id_str;
        this.text = text;
        this.user=user;
    }

    public UserDTO getUser() {
        return this.user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public ZonedDateTime getCreated_at() {
        return this.created_at;
    }

    public void setCreated_at(ZonedDateTime created_at) {
        this.created_at = created_at;
    } 

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getId_str() {
        return this.id_str;
    }

    public void setId_str(String id_str) {
        this.id_str = id_str;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    } 

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof TweetDTO)) {
            return false;
        }
        TweetDTO tweetDTO = (TweetDTO) o;
        return id == tweetDTO.id && Objects.equals(id_str, tweetDTO.id_str) && Objects.equals(text, tweetDTO.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, id_str, text);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", id_str='" + getId_str() + "'" +
            ", created_at='" + getCreated_at() + "'" +
            ", user='" + getUser() + "'" +
            ", text='" + getText() + "'" +
            "}";
    }  

}