package com.zuldigital.challenge.service.dto;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO {
    long id;
    String id_str;
    String name;
    String screen_name;
    String location;

    public UserDTO() {
    }

    public UserDTO(long id, String id_str, String name, String screen_name, String location, String description, String profile_image_url_https) {
        this.id = id;
        this.id_str = id_str;
        this.name = name;
        this.screen_name = screen_name;
        this.location = location;
        this.description = description;
        this.profile_image_url_https = profile_image_url_https;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getId_str() {
        return this.id_str;
    }

    public void setId_str(String id_str) {
        this.id_str = id_str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreen_name() {
        return this.screen_name;
    }

    public void setScreen_name(String screen_name) {
        this.screen_name = screen_name;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfile_image_url_https() {
        return this.profile_image_url_https;
    }

    public void setProfile_image_url_https(String profile_image_url_https) {
        this.profile_image_url_https = profile_image_url_https;
    }

    public UserDTO id(long id) {
        this.id = id;
        return this;
    }

    public UserDTO id_str(String id_str) {
        this.id_str = id_str;
        return this;
    }

    public UserDTO name(String name) {
        this.name = name;
        return this;
    }

    public UserDTO screen_name(String screen_name) {
        this.screen_name = screen_name;
        return this;
    }

    public UserDTO location(String location) {
        this.location = location;
        return this;
    }

    public UserDTO description(String description) {
        this.description = description;
        return this;
    }

    public UserDTO profile_image_url_https(String profile_image_url_https) {
        this.profile_image_url_https = profile_image_url_https;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof UserDTO)) {
            return false;
        }
        UserDTO userDTO = (UserDTO) o;
        return id == userDTO.id && Objects.equals(id_str, userDTO.id_str) && Objects.equals(name, userDTO.name) && Objects.equals(screen_name, userDTO.screen_name) && Objects.equals(location, userDTO.location) && Objects.equals(description, userDTO.description) && Objects.equals(profile_image_url_https, userDTO.profile_image_url_https);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, id_str, name, screen_name, location, description, profile_image_url_https);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", id_str='" + getId_str() + "'" +
            ", name='" + getName() + "'" +
            ", screen_name='" + getScreen_name() + "'" +
            ", location='" + getLocation() + "'" +
            ", description='" + getDescription() + "'" +
            ", profile_image_url_https='" + getProfile_image_url_https() + "'" +
            "}";
    }
    String description;
    String profile_image_url_https;
}